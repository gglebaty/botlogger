package DataBase

import com.bot4s.telegram.models.User

import scala.concurrent.Future

trait UsersStore {
  def insert(user: User): Future[Int]

  def get(id: Seq[Int]): Future[Seq[User]]
}
