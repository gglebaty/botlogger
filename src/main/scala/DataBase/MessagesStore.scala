package DataBase

import java.util.UUID

import Models.Message

import scala.concurrent.Future

trait MessagesStore {
  def insert(message: Message): Future[Int]

  def get(chatId: Long, messageId: Int): Future[Option[Message]]

  def getByUUID(keys: Seq[UUID]): Future[Seq[Message]]

  def drop(key: UUID): Future[Int]

  def getByChatId(chatId: Long): Future[Seq[Message]]

  def forPeriod(chatId: Long, from: Int, until: Int): Future[Seq[Message]]

  def getUserIds(chatId: Long): Future[Seq[Int]]

  def userMessages(chatId: Long, userId: Int): Future[Seq[Message]]

  def statistic(chatId: Long, users: Seq[Int]): Future[Seq[(Int, Int)]]
}
