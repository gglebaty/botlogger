package HMDataBase

import DataBase.DataBase

class HMDataBase extends DataBase{
  val changes = new ChangesHMDataBase
  val chats = new ChatsHMDataBase
  val messages = new MessagesHMDataBase
  val users = new UsersHMDataBase
}
