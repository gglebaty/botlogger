package HMDataBase

import DataBase.UsersStore
import com.bot4s.telegram.models.User

import scala.collection.mutable
import scala.concurrent.Future

class UsersHMDataBase extends UsersStore {
  val users: mutable.HashMap[Int, User] = mutable.HashMap.empty

  def insert(user: User): Future[Int] =
    Future.successful{
      users += (user.id -> user)
      1
    }

  def get(ids: Seq[Int]): Future[Seq[User]] =
    Future.successful(users.values.filter(user => ids.contains(user.id)).toSeq)


}
