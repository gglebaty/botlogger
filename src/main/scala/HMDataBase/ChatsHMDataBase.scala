package HMDataBase

import DataBase.ChatsStore
import Models.Chat

import scala.collection.mutable
import scala.concurrent.Future

class ChatsHMDataBase extends ChatsStore{
  val chats: mutable.HashMap[Long, Chat] = mutable.HashMap.empty

  def insert(chat: Chat): Future[Int] =
    Future.successful{
      chats += (chat.id -> chat)
      1
    }

  def getAll: Future[Seq[Chat]] =
    Future.successful({
      chats.values.toSeq
    })

  def getById(id: Long): Future[Option[Chat]] =
    Future.successful(chats.get(id))
}
