package HMDataBase

import java.util.UUID

import DataBase.ChangesStore
import Models.Change

import scala.concurrent.Future

class ChangesHMDataBase extends ChangesStore{
  var changes: List[Change] = List.empty

  def insert(change: Change): Future[Int] =
    Future.successful{
      changes = change :: changes
      1
    }

  def get(id: UUID): Future[Seq[Change]] =
    Future.successful({
      changes.filter(_.id == id)
    })

  def forPeriod(ids: Seq[UUID], from: Int, until: Int): Future[Seq[Change]] =
    Future.successful({
      changes.filter(change => ids.contains(change.id) && change.date > from && change.date < until)
    })

  def getAllByUUIDs(uuids: Seq[UUID]): Future[Seq[Change]] =
    Future.successful({
      changes.filter(change => uuids.contains(change.id))
    })
}
