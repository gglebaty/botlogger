package Models

import java.util.UUID

case class Message(key: UUID,
                   chatId: Long,
                   messageId: Int,
                   userId: Int,
                   date: Int,
                   text: String,
                   status: MessageStatus.Value)