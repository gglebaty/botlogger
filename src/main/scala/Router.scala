import Bot.Commands
import akka.http.scaladsl.server
import akka.http.scaladsl.server.Directives._
import scala.util.Try

object Router extends Json4sSupport{

  def routes(commands: Commands):server.Route =
    path(s"chats"/Segment) { chatId =>
    get {
      if(Try(chatId.toLong).isSuccess)
        complete(commands.chat(chatId.toLong))
      else complete("Incorrect chatId")
    }
  }~path (s"chats"/Segment/"messages"){ chatId =>
      parameters('user.?, 'from.?, 'until.?) {(param1, param2, param3) =>
        if(Try(chatId.toLong).isSuccess) {
          val command = s"/messages " +
            s"user=${param1.getOrElse("")} " +
            s"from=${param2.getOrElse("")} " +
            s"until=${param3.getOrElse("")}"
          complete(commands.clientCommands(chatId.toLong, command))
        }
        else complete("Incorrect chatId")
      }
  }~path (s"chats"/Segment/"changes"){ chatId =>
      parameters('user.?, 'from.?, 'until.?) {(param1, param2, param3) =>
        if(Try(chatId.toLong).isSuccess) {
          val command = s"/changes " +
            s"user=${param1.getOrElse("")} " +
            s"from=${param2.getOrElse("")} " +
            s"until=${param3.getOrElse("")}"
          complete(commands.clientCommands(chatId.toLong, command))
        }
        else complete("Incorrect chatId")
      }
    } ~path (s"chats"/Segment/Segment){ (chatId, command) =>
      get {
        if(Try(chatId.toLong).isSuccess)
          complete(commands.clientCommands(chatId.toLong, "/" + command))
        else complete("Incorrect chatId")
      }
  }~path(s"chats"){
      get{
        complete(commands.chats())
      }
  }
}