package PostgreSQL

import java.util.UUID

import DataBase.ChangesStore
import Models.Change
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.{ExecutionContext, Future}

class ChangesPostgreSQL(schema: Schema)(implicit e: ExecutionContext) extends ChangesStore {
  def insert(change: Change): Future[Int] =
    schema.db.run(schema.changes += change)

  def get(id: UUID): Future[Seq[Change]] =
    schema.db.run(schema.changes.filter(change => change.id === id).result)

  def forPeriod(ids: Seq[UUID], from: Int, until: Int): Future[Seq[Change]] =
    schema.db.run(schema.changes.filter(change => change.id.inSet(ids) && change.date > from && change.date < until).result)

  def getAllByUUIDs(uuids: Seq[UUID]): Future[Seq[Change]] =
    schema.db.run(schema.changes.filter(change => change.id.inSet(uuids)).result)


}
