package PostgreSQL

import DataBase.UsersStore
import com.bot4s.telegram.models.User
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.{ExecutionContext, Future}

class UsersPostgreSQL(schema: Schema)(implicit e: ExecutionContext) extends UsersStore{
  def insert(user: User): Future[Int] = {
    schema.db.run(schema.users += user)
  }

  def get(id: Seq[Int]): Future[Seq[User]] =
    schema.db.run(schema.users.filter(_.id.inSet(id)).result)

}
