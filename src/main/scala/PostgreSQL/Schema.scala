package PostgreSQL

import java.util.UUID

import Models.{Change, Chat, Message, MessageStatus}
import com.bot4s.telegram.models.{User, ChatType => CType}
import slick.jdbc.JdbcProfile

import scala.concurrent.Future

class Schema(connectionUrl: String, driver: String, profile: JdbcProfile = slick.jdbc.PostgresProfile) {
  import slick.jdbc.PostgresProfile.api._
  implicit val uuidMapper: BaseColumnType[String] = MappedColumnType.base[String, UUID](UUID.fromString, _.toString)

  object ChatType {
    def chatToSqlType(chatType: CType.Value) = chatType.toString
    implicit val ChatColumnType =
      MappedColumnType.base[CType.Value, String](chatToSqlType, CType.withName)
  }

  class Chats(tag: Tag) extends Table[Chat](tag, "Chats"){
    import ChatType._

    def id: Rep[Long] = column[Long]("id", O.PrimaryKey)
    def chatType: Rep[CType.Value] = column[CType.Value]("chatType")
    def title: Rep[Option[String]] = column[Option[String]]("title")
    def username: Rep[Option[String]] = column[Option[String]]("username")
    def firstname: Rep[Option[String]] = column[Option[String]]("firstname")
    def lastname: Rep[Option[String]] = column[Option[String]]("lastname")
    def allMembersAreAdministrators: Rep[Option[Boolean]] = column[Option[Boolean]]("allMembersAreAdministrators")
    def description: Rep[Option[String]] = column[Option[String]]("description")
    def inviteLink: Rep[Option[String]] = column[Option[String]]("inviteLink")

    def * = (id,
      chatType,
      title,
      username,
      firstname,
      lastname,
      allMembersAreAdministrators,
      description,
      inviteLink) <> (Chat.tupled, Chat.unapply)

  }

  val chats = TableQuery[Chats]

  class Users(tag: Tag) extends Table[User](tag, "Users") {
    def id: Rep[Int] = column[Int]("id", O.PrimaryKey)
    def isBot: Rep[Boolean] = column[Boolean]("isBot")
    def firstName: Rep[String] = column[String]("firstName")
    def lastName: Rep[Option[String]] = column[String]("lastName")
    def username: Rep[Option[String]] = column[String]("username")
    def languageCode: Rep[Option[String]] = column[String]("languageCode")

    def * = (id, isBot, firstName, lastName, username, languageCode)<>(User.tupled, User.unapply)
  }

  val users = TableQuery[Users]

  object MessageStatusType {
    def messageStatusToSqlType(messageStatus: MessageStatus.Value) = messageStatus.toString
    implicit val MessageStatusColumnType =
      MappedColumnType.base[MessageStatus.Value, String](messageStatusToSqlType, MessageStatus.withName)
  }

  class Messages(tag: Tag) extends Table[Message](tag, "Messages") {
    import MessageStatusType._

    def key: Rep[UUID] = column[UUID]("key", O.PrimaryKey)
    def chatID: Rep[Long] = column[Long]("chatId")
    def messageId: Rep[Int] = column[Int]("messageId")
    def userId: Rep[Int] = column[Int]("userId")
    def date: Rep[Int] = column[Int]("date")
    def text: Rep[String] = column[String]("text")
    def status: Rep[MessageStatus.Value] = column[MessageStatus.Value]("status")

    def * = (key, chatID, messageId, userId, date, text, status) <> (Message.tupled, Message.unapply)

    def chat = foreignKey("chat_FK", chatID, chats)(_.id)
    def user = foreignKey("user_FK", userId, users)(_.id)
  }

  val messages = TableQuery[Messages]

  class Changes(tag: Tag) extends Table[Change](tag, "Changes"){
    def id: Rep[UUID] = column[UUID]("id")
    def date: Rep[Int] = column[Int]("date")
    def text: Rep[String] = column[String]("text")
    def * = (id, date, text) <> (Change.tupled, Change.unapply)

    def message = foreignKey("message_FK",id,messages)(_.key)
  }

  val changes = TableQuery[Changes]

  val db = Database.forURL(connectionUrl, driver)

  def setup(): Future[Unit] = {
    db.run(users.schema.create)
    db.run(messages.schema.create)
    db.run(changes.schema.create)
    db.run(chats.schema.create)
  }

}
