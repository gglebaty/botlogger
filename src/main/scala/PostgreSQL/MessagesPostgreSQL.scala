package PostgreSQL

import java.util.UUID

import DataBase.MessagesStore
import Models.Message
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.{ExecutionContext, Future}

class MessagesPostgreSQL(schema: Schema)(implicit e: ExecutionContext) extends MessagesStore {
  def insert(message: Message): Future[Int] =
    schema.db.run(schema.messages.insertOrUpdate(message))

  def get(chatId: Long, messageId: Int): Future[Option[Message]] =
    schema.db.run(schema.messages.filter(message => message.chatID === chatId && message.messageId === messageId)
      .take(1).result.headOption)

  def getByUUID(keys: Seq[UUID]): Future[Seq[Message]] =
    schema.db.run(schema.messages.filter(_.key.inSet(keys)).result)

  def drop(key: UUID): Future[Int] =
    schema.db.run(schema.messages.filter(_.key === key).delete)

  def getByChatId(chatId: Long): Future[Seq[Message]] =
    schema.db.run(schema.messages.filter(_.chatID === chatId).result)

  def forPeriod(chatId: Long, from: Int, until: Int): Future[Seq[Message]] =
    schema.db.run(schema.messages.filter(msg => msg.chatID === chatId && msg.date > from && msg.date < until).result)

  def getUserIds(chatId: Long): Future[Seq[Int]] =
    schema.db.run(schema.messages.filter(msg => msg.chatID === chatId).map(msg => msg.userId).result)

  def userMessages(chatId: Long, userId: Int): Future[Seq[Message]] =
    schema.db.run(schema.messages.filter(msg => msg.chatID === chatId && msg.userId === userId).result)

  def statistic(chatId: Long, users: Seq[Int]): Future[Seq[(Int, Int)]] =
    schema.db.run(schema.messages.filter(msg => msg.userId.inSet(users) && msg.chatID === chatId)
      .groupBy(_.userId).map(t => (t._1, t._2.length)).result)

}
