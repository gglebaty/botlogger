package Bot

import java.text.SimpleDateFormat
import java.util.Date

import DataBase.DataBase
import Models.{Change, Chat, Message}
import com.bot4s.telegram.models.User

import scala.concurrent.{ExecutionContext, Future}
import scala.util.matching.Regex

class Commands(repository: DataBase)(implicit e: ExecutionContext) {

  private val parser: SimpleDateFormat = new SimpleDateFormat("dd.MM.yyyy_HH.mm")
  private val begin: Regex = "^/start.*".r
  private val support: Regex = "^/help.*".r
  private val botDescription: Regex = "^/description.*".r
  private val changes: Regex = "^/changes.*".r
  private val users: Regex = "^/users.*".r
  private val statistic: Regex = "^/statistic.*".r
  private val messages: Regex = "^/messages.*".r
  private val usr: Regex = ".*user=(\\d+).*".r
  private val frm: Regex = ".*from=(\\d{2}.\\d{2}.\\d{4}_\\d{2}.\\d{2}).*".r
  private val untl: Regex = ".*until=(\\d{2}.\\d{2}.\\d{4}_\\d{2}.\\d{2}).*".r


  def formattedDate(date: Long): String =
    s"${parser.format( new Date (date * 1000.toLong))}"

  def botCommands(chatId: Long, command: String): Future[String] = {
    command match {
      case begin() => start()
      case support() => help()
      case botDescription() => description()
      case users() => usersAsString(chatId)
      case statistic() => statisticAsString(chatId)
      case changes() => changesAsString(command, chatId)
      case messages() => messagesAsString(command, chatId)
      case _ => Future.successful("There isn't such command")
    }
  }

  def clientCommands(chatId: Long, command: String): Future[Object] = {
    command match {
      case users() => users(chatId)
      case statistic() => statistic(chatId)
      case changes() => changes(command, chatId)
      case messages() => messages(command, chatId)
      case _ => Future.successful("There isn't such route")
    }
  }

  def start(): Future[String] = Future.successful("From now, everything you say plays against you")

  def help(): Future[String] = Future.successful("I can help you manage this bot.\n" +
    "If you’re lost that’s list of bot commands:\n" +
    "/description - bot description\n" +
    "/user - list of active users \n" +
    "/statistic - statistic for each user\n" +
    "/changes - changed messages\n" +
    "/messages - chat messages\n" +
    "If you want to select messages or changes for preset period just run proper command with next parameters:\n" +
    "from=dd.MM.yyyy_HH.mm - start date\n" +
    "until=dd.MM.yyyy_HH.mm - end date\n" +
    "Use parameter user=userId to find messages or changes of given user")

  def description(): Future[String] = Future.successful("LoggerBot is the bot, which collect all the information you write to it")

  def chats(): Future[Seq[Chat]] = repository.chats.getAll

  def messages(command: String, chatId: Long): Future[Seq[Message]] = {
    val from = command match {
      case frm(value) => (parser.parse(value).getTime / 1000).toInt
      case _ => 0
    }
    val until = command match {
      case untl(value) => (parser.parse(value).getTime / 1000).toInt
      case _ => (System.currentTimeMillis()/1000).toInt
    }
    command match {
      case usr(value) => repository.messages.forPeriod(chatId, from, until).map(_.filter(_.userId == value.toInt))
      case _ => repository.messages.forPeriod(chatId, from, until)
    }
  }

  def messagesAsString(command: String, chatId: Long): Future[String] = {
    messages(command, chatId).map(seq =>
      if(seq.isEmpty)
        "No messages"
      else
        seq.map(msg => s"${formattedDate(msg.date)}\n ${msg.text}").mkString("\n")
    )
  }

  def chat(chatId: Long): Future[Option[Chat]]= repository.chats.getById(chatId)

  def changes(command: String, chatId: Long): Future[Seq[(Message,Seq[Change])]] = {
    val from = command match {
      case frm(value) => (parser.parse(value).getTime / 1000).toInt
      case _ => 0
    }
    val until = command match {
      case untl(value) => (parser.parse(value).getTime / 1000).toInt
      case _ => (System.currentTimeMillis()/1000).toInt
    }
    val uuids = command match {
      case usr(user) => repository.messages.userMessages(chatId, user.toInt).map(seq => seq.map(_.key))
      case _ => repository.messages.getByChatId(chatId).map(seq => seq.map(_.key))
    }
    for{
      uuids <- uuids
      changes <- repository.changes.forPeriod(uuids, from, until)
      changesUuids = changes.map(_.id)
      messages <- repository.messages.getByUUID(changesUuids)
      result = messages.map(msg => (msg, changes.filter(_.id == msg.key)))
    } yield result
  }

  def changesAsString(command: String, chatId: Long): Future[String] = {
    for{
      changes <- changes(command, chatId)
      result = changes.map(tuple =>
        s"Message:\n  " +
        s"|${formattedDate(tuple._1.date)}\n  " +
        s"|${tuple._1.text}\n" +
        "Changes:\n" +
        tuple._2.map(change =>
        s"  |${formattedDate(change.date)}\n"+
        s"  | ${change.text}").mkString("\n")).mkString("\n\n")
    } yield result
  }

  def users(chatId: Long): Future[Seq[User]] = {
    for{
      userIds <- repository.messages.getUserIds(chatId)
      users <- repository.users.get(userIds)
    } yield users
  }

  def usersAsString(chatId: Long): Future[String] =
    for {
      users <- users(chatId)
    } yield {
    users.map(user =>
      (user.username, user.lastName) match {
        case (Some(username), Some(lastName)) => s"Id: ${user.id}\nUser $username - ${user.firstName} $lastName"
        case _ => s"User - ${user.firstName}"
      })
      .mkString("\n")
  }


  def statistic(chatId: Long): Future[Seq[(User,Int)]] = {
    for{
      userIds <- repository.messages.getUserIds(chatId)
      users <- repository.users.get(userIds)
      messages <- repository.messages.statistic(chatId, userIds)
      _ <- Future.successful(println(messages))
      result = users.map(user => (user, messages.filter(_._1 == user.id).map(_._2).head))
    } yield result
  }

  def statisticAsString(chatId: Long): Future[String] =
    statistic(chatId).map(_.map(tuple => s"User: ${tuple._1.firstName}\n  Messages:${tuple._2}").mkString("\n"))

}
