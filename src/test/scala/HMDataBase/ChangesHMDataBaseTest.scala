package HMDataBase

import java.util.UUID

import Models.Change
import org.scalatest.{AsyncFlatSpec, Matchers}

class ChangesHMDataBaseTest extends AsyncFlatSpec with Matchers{

  val hmDataBase = new ChangesHMDataBase()

  val uuid: UUID = UUID.randomUUID ()
  val change = Change(uuid, 1, "Test")

  "change" should "be added in changes" in{
    hmDataBase.insert(change)
    hmDataBase.changes should contain(change)
  }

  "method get" should "return right changes of message" in{
    hmDataBase.get(uuid).map(_ shouldBe List(change))
  }

  "method forPeriod" should "return right seq of changes" in {
    hmDataBase.forPeriod(Seq(uuid), 0, 2).map(_ shouldBe Seq(change))
  }

}


