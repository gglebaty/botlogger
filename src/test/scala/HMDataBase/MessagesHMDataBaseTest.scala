package HMDataBase

import java.util.UUID

import Models.{Message, MessageStatus}
import org.scalatest.{AsyncFlatSpec, Matchers}

class MessagesHMDataBaseTest extends AsyncFlatSpec with Matchers{
  val hmDataBase = new MessagesHMDataBase

  val uuid: UUID = UUID.randomUUID ()
  val message: Message = Message(uuid, 1, 1, 1, 1, "Test", MessageStatus.Alive)

  "message" should "be added in messages" in{
    hmDataBase.insert(message)
    hmDataBase.messages should contain(message.key -> message)
  }

  "method get" should "return right message" in{
    hmDataBase.get(1, 1).map(_ shouldBe Some(message))
  }

  "method getByUUID" should "return right chat" in{
    hmDataBase.getByUUID(Seq(uuid)).map(_ shouldBe Seq(message))
  }

  "method dropMessage" should "delete message" in{
    hmDataBase.drop(uuid)
    hmDataBase.messages shouldNot contain(message)
  }

  val uuid2: UUID = UUID.randomUUID()
  val message1 = Message(uuid2, 2, 2, 1, 1, "Test", MessageStatus.Alive)
  hmDataBase.insert(message1)
  val message2 = Message(UUID.randomUUID(), 2, 3, 2, 2, "Test", MessageStatus.Alive)
  hmDataBase.insert(message2)

  "method getByChatId" should "return right list of messages" in{
    hmDataBase.getByChatId(2).map(seq =>{
      seq should contain(message1)
        seq should contain(message2)
    })
  }

  "method forPeriod" should "return right list of messages" in{
    hmDataBase.forPeriod(2, 0, 3).map(seq =>{
      seq should contain(message1)
      seq should contain(message2)
    })
  }

  "method getUserIds" should "return right list of ids" in{
    hmDataBase.getUserIds(2).map(seq =>{
      seq should contain(1)
      seq should contain(2)
    })
  }

  "method userMessages" should "return right list of messages" in{
    hmDataBase.userMessages(2, 2).map(_ shouldBe Seq(message2))
  }


}
