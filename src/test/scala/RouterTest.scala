
import Bot.Commands
import Models.Chat
import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.bot4s.telegram.models.{ChatType, User}
import org.scalamock.scalatest.MockFactory
import org.scalatest.{FlatSpec, Matchers}

import scala.concurrent.Future

class RouterTest extends FlatSpec with Matchers with ScalatestRouteTest with MockFactory{
  val commands: Commands = stub[Commands]

  "Routes" should "return chat messages" in {

    val chat: Chat = Chat(1, ChatType.Private)

    (commands.chat _).when(1).returns(Future.successful(
      Some(chat)
    ))

    Get("/chats/1") ~> Router.routes(commands) ~> check {
      responseAs[String] shouldEqual
        s"""{"id":1,"type":{}}"""
    }
  }


  "Routes" should "return list of chat users" in {
    val user: User = User(1, isBot = false, "Test")

    (commands.clientCommands _).when(1, "/users").returns(Future.successful(
      Seq(user)
    ))
    Get("/chats/1/users") ~> Router.routes(commands) ~> check {
      responseAs[String] shouldEqual
       """[{"id":1,"isBot":false,"firstName":"Test"}]"""
    }
  }

  "Routes" should "return chat statistic" in {
    val user: User = User(1, isBot = false, "Test")

    (commands.clientCommands _).when(1, "/statistic").returns(Future.successful(
      Seq((user,1))
    ))
    Get("/chats/1/statistic") ~> Router.routes(commands) ~> check {
      responseAs[String] shouldEqual
        """[{"_1":{"id":1,"isBot":false,"firstName":"Test"},"_2":1}]"""
    }
  }


  "Routes" should "return list of chats" in {
    val chat = Chat(1, ChatType.Private)

    (commands.chats _).when().returns(Future.successful(
      Seq(chat)
    ))

    Get("/chats") ~> Router.routes(commands) ~> check {
      responseAs[String] shouldEqual
        """[{"id":1,"type":{}}]"""
    }
  }

}
