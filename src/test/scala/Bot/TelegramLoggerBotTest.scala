package Bot

import HMDataBase.HMDataBase
import Logger.Logger
import Models.MessageStatus
import com.bot4s.telegram.models.{Chat, ChatType, User, Message => Msg}
import org.scalatest.{AsyncFlatSpec, Matchers}

class TelegramLoggerBotTest extends AsyncFlatSpec with Matchers{

  val db = new HMDataBase
  val bot = new TelegramLoggerBot(new Logger(db), new Commands(db), "token")

  val message = Msg(1, from = Some(User(1, isBot = true, "Tester")), date = 1, Chat(1, ChatType.Private), text = Some("Test"))
  val editedMessage = Msg(1, from = Some(User(1, isBot = true, "Tester")), date = 1, Chat(1, ChatType.Private), text = Some("Test2"))

  "receiveMessage" should "safe received message" in {
    bot.receiveMessage(message)
    db.messages.get(1, 1).map({
      case Some(msg) =>
        msg.chatId shouldBe 1
        msg.messageId shouldBe 1
        msg.userId shouldBe 1
        msg.text shouldBe "Test"
        msg.status shouldBe MessageStatus.Alive
    })
  }

}
